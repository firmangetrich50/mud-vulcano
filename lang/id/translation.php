<?php

//translation.php
return [
    'back' => 'Kembali',
    'images' => 'Gambar',
    'dashboard' => 'Dashboard',
    'category' => 'Kategori',
    'users' => 'Pengguna',
    'mud-vulcano' => 'Gunung Lumpur',
    'mud-vulcano-images' => 'Gambar Gunung Lumpur',
    'post' => 'Postingan',
    'home' => 'Home',
    'about' => 'Tentang',
    'service' => 'Layanan',
    'service-detail' => 'Layanan Detail',
    'contact' => 'Kontak',
    'title' => 'Judul',
    'slug' => 'Slug',
    'description' => 'Deskripsi',
    'address' => 'Alamat',
    'coordinates' => 'Koordinat',
    'author' => 'Penulis',
    'created_at' => 'Tanggal',
    'select_category' => 'Pilih Kategori',
    'thumbnail' => 'Sampul',
    'content' => 'Konten',
    'user_id' => 'User Id',
    'published' => 'Terbitkan',
    'unpublished' => 'Tidak Terbitkan',
    'action' => 'Aksi',
    'edit' => 'Edit',
    'delete' => 'Hapus',
    'create' => 'Buat',
    'update' => 'Perbarui',
    'cancel' => 'Batal',
    'save' => 'Simpan',
    'are_you_sure' => 'Apakah Anda yakin ?',
    'success' => 'Success',
    'error' => 'Error',
    'success_message' => 'Data berhasil disimpan',
    'error_message' => 'Data gagal disimpan',
    'success_update_message' => 'Data berhasil diperbarui',
    'error_update_message' => 'Data failed to update',
    'success_delete_message' => 'Data berhasil dihapus',
    'error_delete_message' => 'Data gagal dihapus',
    'success_publish_message' => 'Data berhasil diterbitkan',
    'error_publish_message' => 'Data gagal diterbitkan',
    'success_unpublish_message' => 'Data berhasil tidak diterbitkan',
    'error_unpublish_message' => 'Data gagal tidak diterbitkan',
    'success_login_message' => 'Login berhasil',
    'error_login_message' => 'Login gagal',
    'success_logout_message' => 'Logout berhasil',
    'error_logout_message' => 'Logout gagal',
    'success_register_message' => 'Register berhasil',
    'error_register_message' => 'Register gagal',
    'email_or_password_wrong' => 'Email atau Password salah',
    'name' => 'Nama',
    'email' => 'Email',
    'password' => 'Password',
    'confirm_password' => 'Konfirmasi Password',
    'login' => 'Login',
    'remember_me' => 'Remember Me',
    'forgot_password' => 'Forgot Your Password?',
    'register' => 'Register',
    'logout' => 'Logout',
    'reset_password' => 'Reset Password',
];
